
var types = {
	"aberration (shapechanger)":"زائغ (متحوّل)",
	"aberration":"زائغ",
	"beast":"حيوان",
	"celestial (titan)":"سماوي (جبّار)",
	"celestial":"سماوي",
	"construct":"مجسم",
	"dragon":"تنين",
	"elemental":"عناصري",
	"fey":"فيء",
	"fiend (demon)":"شيطان (نمرود)",
	"fiend (demon, orc)":"شيطان (نمرود، أروك)",
	"fiend (demon, shapechanger)":"شيطان (نمرود، متحوّل)",
	"fiend (devil)":"شيطان (إبليس)",
	"fiend (devil, shapechanger)":"شيطان (إبليس، متحّول)",
	"fiend (gnoll)":"شيطان (فرعل)",
	"fiend (shapechanger)":"شيطان (متحوّل)",
	"fiend (yugoloth)":"شيطان (يغلوث)",
	"fiend":"شيطان",
	"giant (cloud giant)":"عملاق (عملاق السحاب)",
	"giant (fire giant)":"عملاق (عملاق النار)",
	"giant (frost giant)":"عملاق (عملاق الصقيع)",
	"giant (hill giant)":"عملاق (عملاق التلال)",
	"giant (stone giant)":"عملاق (عملاق الصخر)",
	"giant (storm giant)":"عملاق (عملاق العاصفة)",
	"giant":"عملاق",
	"humanoid (aarakocra)":"بشري (أراكوكرا)",
	"humanoid (any race)":"بشري (أي عرق)",
	"humanoid (bullywug)":"بشري (علجومي)",
	"humanoid (dwarf)":"بشري (قزم)",
	"humanoid (elf)":"بشري (آلف)",
	"humanoid (firenewt)":"بشري (سمندل ناري)",
	"humanoid (gith)":"بشري (غث)",
	"humanoid (gnoll)":"بشري (فرعل)",
	"humanoid (gnome)":"بشري (غنوم)",
	"humanoid (goblinoid)":"بشري (غوبلن)",
	"humanoid (grimlock)":"بشري (غريملوك)",
	"humanoid (grung)":"بشري (ضفيدع)",
	"humanoid (human)":"بشري (إنس)",
	"humanoid (human, shapechanger)":"بشري (إنس، متحّول)",
	"humanoid (kenku)":"بشري (كنكو)",
	"humanoid (kobold)":"بشري (كوبولد)",
	"humanoid (kuo-toa)":"بشري (كوا-توا)",
	"humanoid (lizardfolk)":"بشري (عظائي)",
	"humanoid (merfolk)":"بشري (غرنقي)",
	"humanoid (orc)":"بشري (أروك)",
	"humanoid (quaggoth)":"بشري (قوغوث)",
	"humanoid (sahuagin)":"بشري (ساهواجين)",
	"humanoid (shapechanger)":"بشري (متحّول)",
	"humanoid (thri-kreen)":"بشري (ثري-كين)",
	"humanoid (troglodyte)":"بشري (تروغلودايت)",
	"humanoid (xvart)":"بشري (زفارت)",
	"humanoid (yuan-ti)":"بشري (يوان-تي)",
	"humanoid":"بشري",
	"monstrosity (shapechanger)":"وحشي (متحوّل)",
	"monstrosity (shapechanger, yuan-ti)":"وحشي (متحوّل، يو ان-تي)",
	"monstrosity (titan)":"وحشي (جبّار)",
	"monstrosity":"وحشي",
	"ooze":"هلامي",
	"plant":"نبات",
	"swarm of Tiny beasts":"سرب من الحيوانات الضئيلة",
	"undead (shapechanger)":"لاميت (متحوّل)",
	"undead":"لاميت"
};

var alignments = {
	"chaotic evil": "فوضوي شرير",
	"chaotic neutral":"فوضوي محايد",
	"chaotic good":"فوضوي خيّر",
	"neutral evil":"محايد شرير",
	"true neutral":"محايد حق",
	"neutral":"محايد",
	"neutral good":"محايد خيّر",
	"lawful evil":"نظامي شرير",
	"lawful neutral":"نظامي محايد",
	"lawful good":"نظامي خيّر",
	"chaotic good evil":"فوضوي خّير/شرير",
	"lawful chaotic evil":"نظامي/فوضوي شرير",
	"unaligned":"بلا انحياز",
	"any non-lawful": "أي غير نظامي",
	"any": "أي",
};

var languages = {
	"giant eagle": "النسر العملاق",
	"worg":"ورغ",
	"winter wolf":"ذئب الشتاء",
	"sahuagin":"ساهواجين",
	"giant owl, understands but cannot speak all but giant owl":"البومة العملاقة، يفهم لكن لا يستطيع التحدث على الإطلاق إلاً بلغة البومة العملاقة",
	"giant elk but can't speak them":"الإلك العملاق لكن لا يستطيع حديثها",
	"understands infernal but can't speak it":"يفهم الجهنمية لكن لا يستطيع حديثها",
	"understands draconic but can't speak":"يفهم التنينية لكن لا يستطيع الحديث",
	"understands common but doesn't speak it":"يفهم العامية لكن لا يستطيع حديثها",
	"understands abyssal but can't speak":"يفهم الجحيمية لكن لا يستطيع الحديث",
	"understands all languages it knew in life but can't speak":"يفهم كل اللغات التي كان يعرفها في حياته لكن لا يستطيع حديثها",
	"understands commands given in any language but can't speak":"يفهم الأوامر المعطاة بأي لغة لكن لا يستطيع الحديث",
	"(can't speak in rat form)":"(لا يستطيع الحديث في شكل الفأر)",
	"(can't speak in boar form)":"(لا يستطيع الحديث في شكل الخنزير البري)",
	"(can't speak in bear form)":"(لا يستطيع الحديث في شكل الدب)",
	"(can't speak in tiger form)":"(لا يستطيع الحديث في شكل النمر)",
	"any one language (usually common)":"أي لغة واحدة (العامية عادة)",
	"any two languages":"أي لغتين إثنتين",
	"any four languages":"أي أربعة لغات",
	"5 other languages":"خمسة لغات أخرى",
	"any, usually common":"أي، عادة العامية",
	"one language known by its creator":"لغة واحدة يعرفها صانعه",
	"the languages it knew in life":"اللغات التي كان يعرفها في حياته",
	"those it knew in life":"تلك التي كان يعرفها في حياته",
	"all it knew in life":"جميع ما كان يعرفه في حياته",
	"any it knew in life":"أي ما كان يعرفه في حياته",
	"all, telepathy 120 ft.":"الكل، التخاطر 36م",
	"telepathy 60 ft.":"التخاطر 18م",
	"telepathy 60ft. (works only with creatures that understand abyssal)":"التخاطر 18م (تعمل فقط مع الكائنات التي تفهم الجحيمية)",
	"telepathy 120 ft.":"التخاطر 36م",
	"but can't speak":"لكن لا يستطيع الحديث",
	"but can't speak it":"لكن لا يستطيع حديثها",
	"choice":"اختيار",
	"understands the languages of its creator but can't speak":"يفهم لغات صانعه لكن لا يستطيع الحديث",
	"understands common and giant but can't speak":"يفهم العامية والعملاقية لكن لا يستطيع الحديث",
	"cannot speak": "لا يستطيع الحديث"
};

var races = {
	"Dragonborn": "نَسْل التنين",
	"Dwarf": "قزم",
	"Hill Dwarf": "قزم التلال",
	"Elf": "آلف",
	"High Elf": "آلف سامي",
	"Gnome": "غنوم",
	"Rock Gnome": "غنوم الصخر",
	"Half Elf": "نصف آلف",
	"Half-elf": "نصف آلف",
	"Halfling": "نصيف",
	"Lightfoot Halfling": "نصيف خفيف القدم",
	"Half Orc": "نصف أروك",
	"HUMAN": "إنسان",
	"Human": "إنسان",
	"Variant Human": "إنسان بديل",
	"Tiefling": "تيفلنغ"
};

var rarity = {
	"Common": "شائع",
	"Uncommon": "غير شائع",
	"Rare": "نادر",
	"Very rare": "نادر جداً",
	"Legendary": "أسطوري"
};

function round(num) {
	return Math.round((num + Number.EPSILON) * 100) / 100;
}

function footsToMeters(ft) {
	return round(parseInt(ft)*0.3);
}

function milesToMeters(mi) {
	return round(parseInt(mi)*1.5);
}

function parseSenses(sensesText) {
	const senses = sensesText.split('. ');
	let parsed = '';
	senses.forEach(sense => { parsed = parseSense(sense) + ' ' + parsed; });
	return parsed;
}

function parseSense(sense) {
	var regexp = /([0-9]+)/gi;
	sense = sense.replace(/ft/gi, 'م');
	sense = sense.replace(/feet/gi, 'م');
	sense = sense.replace(/Darkvision/gi, "رؤية ظلامية");
	sense = sense.replace(/Darvision/gi, "رؤية ظلامية"); //bug ^^
	sense = sense.replace(/Blindsight/gi, "رؤية عمياء");
	sense = sense.replace(/Truesight/gi, "رؤية حقيقية");
	sense = sense.replace(/tremorsense/gi, "حاسة إهتزازية");
	sense = sense.replace(/Blind Beyond/gi, "أعمى ما وراء");
	sense = sense.replace(/this radius/gi, "هذا القطر");
	sense = sense.replace((sense.match(regexp)), footsToMeters(sense.match(regexp)));
	sense = sense.replace("(blind beyond this radius)", "(أعمى ما وراء هذا القطر)");
	return sense;
}

function parseDamage(damage) {
	damage = damage.replace(/bludgeoning/gi, 'خبط');
	damage = damage.replace(/piercing/gi, 'طعن');
	damage = damage.replace(/and/gi, 'و');
	damage = damage.replace(/slashing/gi, 'قطع');
	damage = damage.replace(/from/gi, 'من');
	damage = damage.replace(/nonmagical attacks/gi, 'هجمات غير سحرية');
	damage = damage.replace(/that aren't silvered/gi, 'الغير مقضضة');
	damage = damage.replace(/not made with silvered weapons/gi, 'ليست من قبل أسلحة مفضضة');
	return damage;
}

Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		
		Babele.get().register({
			module: 'FoundryVTT-dnd5e-ar',
			lang: 'ar',
			dir: 'compendium'
		});

		Babele.get().registerConverters({
			"weight": (value) => { return parseInt(value)/2 },
			"range": (range) => {
				if(range) {
					if(range.units === 'ft') {
						if(range.long) {
							range = mergeObject(range, { long: footsToMeters(range.long) });
						}
						return mergeObject(range, { value: footsToMeters(range.value) });
					}
					if(range.units === 'mi') {
						if(range.long) {
							range = mergeObject(range, { long: milesToMeters(range.long) });
						}
						return mergeObject(range, { value: milesToMeters(range.value) });
					}
					return range;
				}
			},
			"type": (type) => {
				return types[type.toLowerCase()];
			},
			"alignement": (alignment) => {
				return alignments[alignment.toLowerCase()];
			},
			"movement": (movement) => {
				let convert = (value) => { return value; };
				let units = movement.units;
				if(units === 'ft') {
					convert = (value) => { return footsToMeters(value) };
					units = "m";
				}
				if(units === 'ml') {
					convert = (value) => { return milesToMeters(value) };
					units = "m";
				}

				return mergeObject(movement, {
					burrow: convert(movement.burrow),
					climb: convert(movement.climb),
					fly: convert(movement.fly),
					swim: convert(movement.swim),
					units: units,
					walk: convert(movement.walk)
				});
			},
			"senses": (senses) => {
				return senses ? parseSenses(senses) : null;
			},
			"di": (damage) => {
				return parseDamage(damage);
			},
			"languages": (lang) => {
				if (lang != null ) {
					const languagesSplit = lang.split('; ');
					var languagesFin = '';
					var languagesTr = '';
					languagesSplit.forEach(function(el){
						languagesTr = languages[el.toLowerCase()] ;
						if (languagesTr != null) {
							if (languagesFin === '') {
								languagesFin = languagesTr;
							}  else {
								languagesFin = languagesFin + ' ; '  + languagesTr;
							}
						}
					});
					return languagesFin;
				}
			},
			"token": (token) => {
				mergeObject(
					token, {
						dimSight: footsToMeters(token.dimSight),
						brightSight: footsToMeters(token.brightSight)
					}
				);
			},
			"race": (race) => {
				return races[race] ? races[race] : race;
			},
			"rarity": (r) => {
				return rarity[r] ? rarity[r] : r
			}
		});

		CONFIG.DND5E.encumbrance.currencyPerWeight = 100;
		CONFIG.DND5E.encumbrance.strMultiplier = 7.5;

		CONFIG.DND5E.movementUnits = {
			"m": "Metri",
			"km": "Km"
		}
	}
});

Hooks.on('preCreateScene', (scene) => {
	mergeObject(scene, { "gridUnits": "m", "gridDistance": 1.5 });
});

Hooks.on('createActor', (actor) => {
	if(actor.getFlag("babele", "translated")) {
		return;
	}
	actor.update({
		token: {
			dimSight: footsToMeters(actor.data.token.dimSight),
			brightSight: footsToMeters(actor.data.token.brightSight)
		},
		data: {
			attributes: {
				movement: {
					burrow: 0,
					climb: 0,
					fly: 0,
					swim: 0,
					units: 'm',
					walk: 9
				}
			}
		}
	});
})

async function skillSorting() {
	const lists = document.getElementsByClassName("skills-list");
	for (let list of lists) {
		const competences = list.childNodes;
		let complist = [];
		for (let sk of competences) {
			if (sk.innerText && sk.tagName == "LI") {
				complist.push(sk);
			}
		}
		complist.sort(function(a, b) {
			return (a.innerText > b.innerText) ? 1 : -1;
		});
		for (let sk of complist) {
			list.appendChild(sk)
		}
	}
}

Hooks.on("renderActorSheet", async function() {
	skillSorting();
});
